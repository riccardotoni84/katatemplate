import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class TestExample {

//    private Rover rover;
    private Game game;

    @Before
    public void initialise(){
//        rover = new Rover();
        game = new Game();
    }

    @Test
    @Parameters({
            "4, 2, WON-LOSE",
            "4, 3, ADVANTAGE-",
            "4, 4, DEUCE",

    })

    public void getGameResultForScoring(int scoreFirstPLayer,
                                        int scoreSecondPLayer,
                                        String result)
    {
        assertThat(game.getResult(scoreFirstPLayer, scoreSecondPLayer),
                Is.is(result));
    }
}
